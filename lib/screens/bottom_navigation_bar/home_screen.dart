import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movies_app_clone/blocs/movies_bloc.dart';
import 'package:movies_app_clone/models/item_model.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    bloc.fetchAllMovies();
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          actions: [],
        ),
        body: SafeArea(
          child: Stack(children: [
            Padding(
              padding: EdgeInsets.only(left: 8.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Row(
                      children: [
                        StreamBuilder(
                            stream: bloc.allMovies,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return Container(
                                  width: MediaQuery.of(context).size.width - 16,
                                  height: MediaQuery.of(context).size.width/2,
                                  // color: Colors.red,
                                  child: ItemsLoad(snapshot),
                                );
                              } else if (!snapshot.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                            })
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "For You",
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.keyboard_arrow_right),
                          color: Colors.grey,
                          onPressed: () {},
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        StreamBuilder(
                            stream: bloc.allMovies,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return Container(
                                  width: MediaQuery.of(context).size.width/2,
                                  height: MediaQuery.of(context).size.width/3,
                                  color: Colors.red,
                                );
                              } else if (!snapshot.hasData) {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                            })
                      ],
                    )
                  ],
                ),
              ),
            )
          ]),
        ));
  }
}

class ItemsLoad extends StatefulWidget {
  AsyncSnapshot<ItemModel> snapshot;
  ItemsLoad(this.snapshot);

  @override
  _ItemsLoadState createState() => _ItemsLoadState();
}

class _ItemsLoadState extends State<ItemsLoad> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(scrollDirection: Axis.horizontal, itemCount: widget.snapshot.data.results.length,itemBuilder: (context,int index) {
      return Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
          width: MediaQuery.of(context).size.width/3,
          height: 300,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              image: DecorationImage(
                  image: NetworkImage(widget.snapshot.data.results[index].posterPath),
                  fit: BoxFit.cover,
                  colorFilter:
                  ColorFilter.mode(Colors.black.withOpacity(0.1),
                      BlendMode.darken)
              )
          ),
        ),
      );
    });
  }
}

// Image.network(widget.snapshot.data.results[index].posterPath)