import 'package:flutter/material.dart';
import 'package:movies_app_clone/screens/bottom_navigation_bar/explore_screen.dart';
import 'package:movies_app_clone/screens/bottom_navigation_bar/me_screen.dart';
import 'package:movies_app_clone/screens/bottom_navigation_bar/stories_screen.dart';
import 'package:movies_app_clone/screens/bottom_navigation_bar/home_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentTab;
  List _screens = [
    HomeScreen(),
    ExploreScreen(),
    StoriesScreen(),
    MeScreen(),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentTab = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: _screens.elementAt(_currentTab),
        ),
      ),
      bottomNavigationBar: Row(
        children: [
          buildNavBarItem(Icons.home, 0, "Home"),
          buildNavBarItem(Icons.explore, 1, "Explore"),
          buildNavBarItem(Icons.remove_red_eye_sharp, 2, "Stories"),
          buildNavBarItem(Icons.person, 3, "Me"),
        ],
      ),
    );
  }

  Widget buildNavBarItem(IconData icon, int index, String iconName) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _currentTab = index;
        });
      },
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 4,
        decoration: index == _currentTab
            ? BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        width: 4, color: Theme.of(context).accentColor)),
                // gradient: LinearGradient(colors: [
                //   Theme.of(context).accentColor.withOpacity(0.3),
                //   Theme.of(context).accentColor.withOpacity(0.015),
                // ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
              )
            : BoxDecoration(),
        child: Column(
          children: [
            Icon(
              icon,
              color: index == _currentTab
                  ? Theme.of(context).accentColor
                  : Colors.grey,
            ),
            Text(
              iconName,
              style: TextStyle(
                  color: index == _currentTab
                      ? Theme.of(context).accentColor
                      : Colors.grey,
                  fontSize: 12.0),
            )
          ],
        ),
      ),
    );
  }
}
