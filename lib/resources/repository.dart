import 'package:movies_app_clone/models/item_model.dart';
import 'package:movies_app_clone/resources/movie_api_provider.dart';

class Repository {
  final movieapiprovider = MovieApiProvider();

  Future<ItemModel> fetchAllMovies() => movieapiprovider.fetchMovieList();
}