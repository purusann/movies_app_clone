import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:movies_app_clone/models/item_model.dart';

class MovieApiProvider {
  Client client = Client();
  final apiKey = "b423f9f60d9a8834fc6e8859d0bb716a";
  final baseUrl = "http://api.themoviedb.org/3/movie";

  Future<ItemModel> fetchMovieList() async {
    print("Fetched movies list");
    final response = await client.get(Uri.parse("https://api.themoviedb.org/3/movie/now_playing?api_key="+apiKey));
    print(response.body.toString());
    if (response.statusCode == 200) {
      return ItemModel.fromJson(json.decode(response.body));
    } else {
      throw Exception("Failed to load post");
    }
  }

}