import 'package:movies_app_clone/models/item_model.dart';
import 'package:movies_app_clone/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class MovieBloc {
  final repository = Repository();
  final movieFetcher = PublishSubject<ItemModel>();
  Stream<ItemModel> get allMovies => movieFetcher.stream;

  fetchAllMovies() async{
    ItemModel itemModel = await repository.fetchAllMovies();
    movieFetcher.sink.add(itemModel);
  }

  dispose() {
    movieFetcher.close();
  }

}

final bloc = MovieBloc();