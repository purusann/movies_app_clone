import 'package:flutter/material.dart';
import 'package:movies_app_clone/blocs/movies_bloc.dart';
import 'package:movies_app_clone/models/item_model.dart';
import 'package:movies_app_clone/screens/main_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // builder: (context, child) => SafeArea(child: child),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ).copyWith(
          primaryColor: Color(0xFF3EBACE),
          accentColor: Color(0xFFFF6022),
          scaffoldBackgroundColor: Color(0xFFF3F5F7)
          // Color(0xFFFF6022),
          ),
      home: MainScreen(),
    );
  }
}

